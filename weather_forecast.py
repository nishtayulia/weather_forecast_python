#@weather_forecast_python_bot  --> telegrambot
import telebot
import requests

from bs4 import BeautifulSoup as Bs
from telebot import types

token = '5777281370:AAHIgPaosrWML8EMX2045OakdGl84GWNOc0'

bot = telebot.TeleBot(token=token)

@bot.message_handler(commands=['start'])
def main_func(message):
    user = bot.send_message(message.chat.id, text='What is the weather forecast in your city?',
                     reply_markup=keyboard())
    bot.register_next_step_handler(user, func_for_option)

def func_for_option(message):
    if message.text == 'Weather in Kyiv':
        weather_func(message)
    elif message.text == 'Enter your city':
        city_func(message)

def weather_func(message):
    url = f'https://ua.sinoptik.ua/погода-київ'
    response = requests.get(url)
    html = Bs(response.content, 'html.parser')
    lst_img = [i.get("src") for i in html.find_all('img')]
    response_img = requests.get(f'https:{lst_img[8]}')

    with open('image.jpg', 'wb') as file:
        file.write(response_img.content)
    bot.send_sticker(message.chat.id,
                     sticker=open('image.jpg', 'rb'))

    for i in html.select('#bd2'):
        temp_min = i.select('.temperature > .min > span')[0].text
        temp_max = i.select('.temperature > .max > span')[0].text
        bot.send_message(message.chat.id, text=f'min: {temp_min}\n'
                         f'max: {temp_max}')

    for j in html.select('#bd1c'):
        description = j.select('.description' )[0].text
        bot.send_message(message.chat.id, text=f' {description}')

def city_func(message):
    buttons = types.ReplyKeyboardRemove(selective=False)
    city = bot.send_message(message.chat.id,
                            text='Введіть назву міста: ',
                            reply_markup=buttons)
    bot.register_next_step_handler(city, retes_func)

def retes_func(message):
    url = f'https://ua.sinoptik.ua/погода-{message.text}'
    response = requests.get(url)
    html = Bs(response.content, 'html.parser')
    lst_img = [i.get("src") for i in html.find_all('img')]
    response_img = requests.get(f'https:{lst_img[8]}')

    with open('image.jpg', 'wb') as file:
        file.write(response_img.content)
    bot.send_sticker(message.chat.id,
                     sticker=open('image.jpg', 'rb'))

    for i in html.select('#bd2'):
        temp_min = i.select('.temperature > .min > span')[0].text
        temp_max = i.select('.temperature > .max > span')[0].text
        bot.send_message(message.chat.id, text=f'min: {temp_min}\n'
                                               f'max: {temp_max}')

    for j in html.select('#bd1c'):
        description = j.select('.description')[0].text
        bot.send_message(message.chat.id, text=f' {description}')

def keyboard():
    buttons = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    button1 = types.KeyboardButton(text='Weather in Kyiv')
    button2 = types.KeyboardButton(text='Enter your city')
    buttons.add(button1, button2)
    return buttons


if __name__=='__main__':
    bot.polling()